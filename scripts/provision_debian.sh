#!/bin/bash

# XNAT is developed by the Neuroinformatics Research Group, https://nrg.wustl.edu
# https://www.xnat.org
# Copyright (c) 2015-2021, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# XNAT Stack Base Box
#
# This Vagrant project creates a VM that can be saved as the XNAT Stack base box. This process started from the advice
# from this article:
#
#  * https://scotch.io/tutorials/how-to-create-a-vagrant-base-box-from-an-existing-one#ssh-into-the-box-and-customize-it
#
# This configuration script is intended to provision most Debian/Ubuntu-based operating systems, but has only been
# verified on the following specific platforms and versions:
#
# * Ubuntu 18.04
# * Ubuntu 20.04
#
# This particular implementation creates a base VM then adds a number of XNAT prerequisites and useful utilities,
# including:
#
#  * Neurodebian
#  * Ruby
#  * Git
#  * Zip
#  * OpenJDK 1.8
#  * Tomcat 9
#  * nginx
#  * PostgreSQL 12
#  * Docker
#  * Tools including vim and tmux
#
# The full documentation for the XNAT Stack base box can be found in the accompanying README.md. License information
# can be found in LICENSE.txt.
#

# Create APT_CMD alias to specify non-interactive front-end for Debian to eliminate annoying message about stdin.
APT_CMD="sudo DEBIAN_FRONTEND=noninteractive apt-get"

# Install dependencies: configure NeuroDebian, update and upgrade the VM, then install PostgreSQL, OpenJDK 8, Tomcat 9, and nginx.

source /etc/lsb-release
echo "Now building xnatstack on platform ${DISTRIB_ID} ${DISTRIB_RELEASE}: ${DISTRIB_DESCRIPTION}"

# Run update and install these first so that we can add APT repos before doing another update and full-upgrade.
${APT_CMD} update

OS_VERSION=$(lsb_release -cs)

${APT_CMD} --assume-yes install software-properties-common

echo
echo "------------------------------------------------------------"
echo "Adding NeuroDebian"
echo "------------------------------------------------------------"
sudo wget -O /etc/apt/sources.list.d/neurodebian.sources.list https://neuro.debian.net/lists/${OS_VERSION}.us-nh.libre 2>&1
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv-keys 0xA5D32F012649A5A9 2>&1

# Setup Postgresql repo so we can get desired or latest version
echo
echo "------------------------------------------------------------"
echo "Configuring PostgreSQL repo to fetch latest version"
echo "------------------------------------------------------------"
echo "deb [arch=amd64] https://apt.postgresql.org/pub/repos/apt/ ${OS_VERSION}-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv-keys 0x7FCC7D46ACCC4CF8 2>&1

echo
echo "------------------------------------------------------------"
echo "Adding Docker GPG key and APT repository"
echo "------------------------------------------------------------"
echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${OS_VERSION} stable" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv-keys 0x7EA0A9C3F273FCD8 2>&1

echo
echo "------------------------------------------------------------"
echo "Updating package lists and running full platform upgrade"
echo "------------------------------------------------------------"
${APT_CMD} update
${APT_CMD} --assume-yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" full-upgrade

echo
echo "------------------------------------------------------------"
echo "Checking for build headers for updated kernel"
echo "------------------------------------------------------------"
REQ_HEADERS="linux-headers-$(ls -l /boot/vmlinuz | sed -E 's/^.* -> vmlinuz-(.*)$/\1/')"
[[ $(dpkg --get-selections | grep ${REQ_HEADERS} | wc -l) == 0 ]] && { echo; echo "Installing ${REQ_HEADERS}"; ${APT_CMD} --assume-yes install ${REQ_HEADERS}; }

echo
echo "------------------------------------------------------------"
echo "Installing Docker CE and required support packages"
echo "------------------------------------------------------------"
${APT_CMD} --assume-yes install apt-transport-https ca-certificates curl linux-image-extra-virtual docker-ce

echo
echo "------------------------------------------------------------"
echo "Installing OpenJDK 8"
echo "------------------------------------------------------------"
${APT_CMD} --assume-yes install openjdk-8-jdk

echo
echo "------------------------------------------------------------"
echo "Installing Nginx"
echo "------------------------------------------------------------"
${APT_CMD} --assume-yes install nginx

echo
echo "------------------------------------------------------------"
echo "Installing Tomcat 9"
echo "------------------------------------------------------------"
${APT_CMD} --assume-yes install tomcat9 tomcat9-admin

echo
echo "------------------------------------------------------------"
echo "Installing Postgres 12"
echo "------------------------------------------------------------"
echo "Executing: ${APT_CMD} --assume-yes install postgresql-12 postgresql-server-dev-12"
${APT_CMD} --assume-yes install postgresql-12 postgresql-server-dev-12

echo
echo "------------------------------------------------------------"
echo "Installing Postgres 12"
echo "------------------------------------------------------------"
echo "Executing: ${APT_CMD} --assume-yes install postgresql-12 postgresql-server-dev-12"
${APT_CMD} --assume-yes install postgresql-12 postgresql-server-dev-12

echo
echo "------------------------------------------------------------"
echo "Installing other tools"
echo "------------------------------------------------------------"
${APT_CMD} --assume-yes install chrony dcm2niix dcmtk git libinsighttoolkit4-dev libpython3-dev libxml2-dev libxslt1-dev mercurial mricron python3-lxml python3-pip python3-testresources python3-setuptools ruby tmux vim zip dkms build-essential

echo
echo "------------------------------------------------------------"
echo "Installing and updating Python tools"
echo "------------------------------------------------------------"
sudo pip3 install --upgrade pip
sudo pip3 install --upgrade requests pyxnat xnat httpie

echo

${APT_CMD} autoremove
${APT_CMD} clean

# Zero out the blank space then clear it.
echo
echo "------------------------------------------------------------"
echo "Zeroing out and clearing blank space. An error message will"
echo "appear:"
echo
echo "  dd: error writing ‘/EMPTY’: No space left on device"
echo
echo "This is expected."
echo "------------------------------------------------------------"
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -f /EMPTY

# Clear out the history.
cat /dev/null > ~/.bash_history && history -c && exit
